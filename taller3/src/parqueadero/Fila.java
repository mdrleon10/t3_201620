package parqueadero;

import java.util.Iterator;

public class Fila <T> implements Iterable<T>
{ 
	private Node primero;
	private Node ultimo;
	private int size; 


	private class Node
	{
		T t;
		Node next; 
	}

	public Fila()
	{
		primero = null;
		ultimo = null;
		size = 0; 
	}

	public boolean estaVacio()
	{
		return primero == null;
	}

	public void enqueue(T objeto)
	{
		Node ultimoViejo = ultimo;
		ultimo = new Node();
		ultimo.t =objeto;
		ultimo.next = null;
		if (estaVacio())
		{
			primero = ultimo;

		}
		else 
		{
			ultimoViejo.next = ultimo; 
		}
		size++;
	}

	public T dequeue ()
	{
		T item = primero.t;
		primero = primero.next;
		if (estaVacio())
		{
			ultimo = null;
		}
		return item; 
	}

	public int darTama�o()
	{
		return size;
	}


	public Iterator<T> iterator() 
	{
		return new IteradorLista();
	}

	private class IteradorLista implements Iterator<T>
	{
		private Node actual = primero;

		public boolean hasNext() {
			return actual != null;
		}

		public T next() {
			if (!hasNext())
			{
				throw new UnsupportedOperationException();
			}
			T item = actual.t;
			actual = actual.next;
			return item;
		}

		public void remove() 
		{
			throw new UnsupportedOperationException();
		}
	}

	public T darPrimero() {
		// TODO Auto-generated method stub
		return null;
	}
}
