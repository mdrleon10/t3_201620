package parqueadero;

import java.util.Iterator;

public class Pila <T>
{
	private Node primero = null;
	
	private class Node
	{
		T t; 
		Node next;
	}
	
	public boolean estaVacio()
	{
		return primero == null;
	}
	
	public void push(T objeto)
	{
		Node ultimoViejo = primero;
		primero = new Node();
		primero.t = objeto;
		primero.next = ultimoViejo;
	}
	
	public T pop()
	{
		T item = primero.t;
		primero = primero.next;
		return item;
	}
	
	public Iterator<T> iterator() 
	{
		return new IteradorLista();
	}
	
	private class IteradorLista implements Iterator<T>
	{
		private Node actual = primero;

		public boolean hasNext() {
			return actual != null;
		}

		public T next() {
			if (!hasNext())
			{
				throw new UnsupportedOperationException();
			}
			T item = actual.t;
			actual = actual.next;
			return item;
		}

		public void remove() 
		{
			throw new UnsupportedOperationException();
		}
	}
	

}
