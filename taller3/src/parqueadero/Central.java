package parqueadero;

import java.util.Date;
import java.util.Iterator;


public class Central 
{
	/**
	 * Cola de carros en espera para ser estacionados
	 */
	private Fila<Carro> enEspera;

	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	private Pila<Carro> park1;

	private Pila<Carro> park2;

	private Pila<Carro> park3;

	private Pila<Carro> park4;

	private Pila<Carro> park5;

	private Pila<Carro> park6;

	private Pila<Carro> park7;

	private Pila<Carro> park8;

	/**
	 * Pila de carros parqueadero temporal:
	 * Aca se estacionan los carros temporalmente cuando se quiere
	 * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
	 */

	private Pila<Carro> temporal;

	/**
	 * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
	 */
	public Central ()
	{
		park1 = new Pila<Carro>();
		park2 = new Pila<Carro>();
		park3 = new Pila<Carro>();
		park4 = new Pila<Carro>();
		park5 = new Pila<Carro>();
		park6 = new Pila<Carro>();
		park7 = new Pila<Carro>();
		park8 = new Pila<Carro>();
		temporal = new Pila<Carro>();
		enEspera = new Fila<Carro>();
	}
	
	public Fila darFila()
	{
		return enEspera;
	}
	
	public Pila darPila1()
	{
		return park1;
	}

	public Pila darPila2()
	{
		return park2;
	}
	
	public Pila darPila3()
	{
		return park3;
	}
	
	public Pila darPila4()
	{
		return park4;
	}
	
	public Pila darPila5()
	{
		return park5;
	}
	
	public Pila darPila6()
	{
		return park6;
	}
	
	public Pila darPila7()
	{
		return park7;
	}
	
	public Pila darPila8()
	{
		return park8;
	}
	/**
	 * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
	 * @param pColor color del vehiculo
	 * @param pMatricula matricula del vehiculo
	 * @param pNombreConductor nombre de quien conduce el vehiculo
	 */
	public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
	{
		enEspera.enqueue(new Carro(pColor, pMatricula, pNombreConductor));
	}    

	/**
	 * Parquea el siguiente carro en la cola de carros por parquear
	 * @return matricula del vehiculo parqueado y ubicaci�n
	 * @throws Exception
	 */
	public String parquearCarroEnCola() throws Exception
	{
		if(enEspera.estaVacio())
		{
			throw new Exception ("No hay mas carros por parquear");
		}
		String mensaje = parquearCarro(enEspera.darPrimero());
		enEspera.dequeue();
		return "El carro con matricula " + enEspera.darPrimero().darMatricula() + " se ha parqueado en el " + mensaje;
	} 
	/**
	 * Saca del parqueadero el vehiculo de un cliente
	 * @param matricula del carro que se quiere sacar
	 * @return El monto de dinero que el cliente debe pagar
	 * @throws Exception si no encuentra el carro
	 */
	public double atenderClienteSale (String matricula) throws Exception
	{
		Carro sacado = sacarCarro(matricula);
		if(sacado == null)
		{
			throw new Exception("No se encontro el carro buscado");
		}
		return cobrarTarifa(sacado);
	}

	/**
	 * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el carro
	 * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
	 * @return El parqueadero en el que qued� el carro
	 * @throws Exception
	 */
	public String parquearCarro(Carro aParquear) throws Exception
	{
		int park = 1;
		if(!park1.estaVacio())
		{
			Iterator<Carro> itr = park1.iterator();
			int cont = 0;
			while(itr.hasNext()){
				cont++;
				Carro temp = itr.next();
			}
			if(cont < 4)
			{
				park = 1;
				park1.push(aParquear);
			}
		}
		else if(!park2.estaVacio())
		{
			Iterator<Carro> itr = park2.iterator();
			int cont = 0;
			while(itr.hasNext()){
				cont++;
				Carro temp2 = itr.next();
			}
			if(cont < 4)
			{
				park = 2;
				park2.push(aParquear);
			}
		}
		else if(!park3.estaVacio())
		{
			Iterator<Carro> itr = park3.iterator();
			int cont = 0;
			while(itr.hasNext()){
				cont++;
				Carro temp3 = itr.next();
			}
			if(cont < 4)
			{
				park = 3;
				park3.push(aParquear);
			}
		}
		else if(!park4.estaVacio())
		{
			Iterator<Carro> itr = park4.iterator();
			int cont = 0;
			while(itr.hasNext()){
				cont++;
				Carro temp4 = itr.next();
			}
			if(cont < 4)
			{
				park = 4;
				park4.push(aParquear);
			}
		}
		else if(!park5.estaVacio())
		{
			Iterator<Carro> itr = park5.iterator();
			int cont = 0;
			while(itr.hasNext()){
				cont++;
				Carro temp5 = itr.next();
			}
			if(cont < 4)
			{
				park = 5;
				park5.push(aParquear);
			}
		}
		else if(!park6.estaVacio())
		{
			Iterator<Carro> itr = park6.iterator();
			int cont = 0;
			while(itr.hasNext()){
				cont++;
				Carro temp6 = itr.next();
			}
			if(cont < 4)
			{
				park = 6;
				park6.push(aParquear);
			}
		}
		else if(!park7.estaVacio())
		{
			Iterator<Carro> itr = park7.iterator();
			int cont = 0;
			while(itr.hasNext()){
				cont++;
				Carro temp7 = itr.next();
			}
			if(cont < 4)
			{
				park = 7;
				park7.push(aParquear);
			}
		}
		else if(!park8.estaVacio())
		{
			Iterator<Carro> itr = park8.iterator();
			int cont = 0;
			while(itr.hasNext()){
				cont++;
				Carro temp8 = itr.next();
			}
			if(cont < 4)
			{
				park = 8;
				park8.push(aParquear);
			}
		}
		
		return "PARQUEADERO " + park;    	    
	}

	/**
	 * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
	 * @param matricula del vehiculo que se quiere sacar
	 * @return el carro buscado
	 */
	public Carro sacarCarro (String matricula)
	{
		Carro buscado = null;
		if(sacarCarroP1(matricula) != null)
		{
			buscado = sacarCarroP1(matricula);	
		}
		else if(sacarCarroP2(matricula)!= null)
		{
			buscado = sacarCarroP2(matricula);
		}
		else if(sacarCarroP3(matricula)!= null)
		{
			buscado = sacarCarroP3(matricula);
		}
		else if(sacarCarroP4(matricula)!= null)
		{
			buscado = sacarCarroP4(matricula);
		}
		else if(sacarCarroP5(matricula)!= null)
		{
			buscado = sacarCarroP5(matricula);
		}
		else if(sacarCarroP6(matricula)!= null)
		{
			buscado = sacarCarroP6(matricula);
		}
		else if(sacarCarroP7(matricula)!= null)
		{
			buscado = sacarCarroP7(matricula);
		}
		else if(sacarCarroP8(matricula)!= null)
		{
			buscado = sacarCarroP8(matricula);
		}

		return buscado;
	}

	/**
	 * Saca un carro del parqueadero 1 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP1 (String matricula)
	{
		Carro buscado = null;
		if(!park1.estaVacio()){
			Iterator itr = park1.iterator();
			while(itr.hasNext())
			{

				Carro temp = (Carro)itr.next();
				if(temp.darMatricula().equals(matricula)){
					buscado = temp;
					park1.pop();
				}
				else 
				{
					temporal.push(park1.pop());
				}
			}
			Iterator itr2 = temporal.iterator();
			while (itr.hasNext())
			{
				park1.push(temporal.pop());
				Carro temp2 = (Carro) itr.next();
			}
		}
		return buscado;

	}

	/**
	 * Saca un carro del parqueadero 2 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP2 (String matricula)
	{
		Carro buscado = null;
		if(!park2.estaVacio()){
			Iterator itr = park2.iterator();
			while(itr.hasNext())
			{

				Carro temp = (Carro)itr.next();
				if(temp.darMatricula().equals(matricula)){
					buscado = temp;
					park2.pop();
				}
				else 
				{
					temporal.push(park2.pop());
				}
			}
			Iterator itr2 = temporal.iterator();
			while (itr.hasNext())
			{
				park2.push(temporal.pop());
				Carro temp2 = (Carro) itr.next();
			}
		}
		return buscado;
	}

	/**
	 * Saca un carro del parqueadero 3 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP3 (String matricula)
	{
		Carro buscado = null;
		if(!park3.estaVacio()){
			Iterator itr = park3.iterator();
			while(itr.hasNext())
			{

				Carro temp = (Carro)itr.next();
				if(temp.darMatricula().equals(matricula)){
					buscado = temp;
					park3.pop();
				}
				else 
				{
					temporal.push(park3.pop());
				}
			}
			Iterator itr2 = temporal.iterator();
			while (itr.hasNext())
			{
				park3.push(temporal.pop());
				Carro temp2 = (Carro) itr.next();
			}
		}
		return buscado;
	}
	/**
	 * Saca un carro del parqueadero 4 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP4 (String matricula)
	{

		Carro buscado = null;
		if(!park4.estaVacio()){
			Iterator itr = park4.iterator();
			while(itr.hasNext())
			{

				Carro temp = (Carro)itr.next();
				if(temp.darMatricula().equals(matricula)){
					buscado = temp;
					park4.pop();
				}
				else 
				{
					temporal.push(park4.pop());
				}
			}
			Iterator itr2 = temporal.iterator();
			while (itr.hasNext())
			{
				park4.push(temporal.pop());
				Carro temp2 = (Carro) itr.next();
			}
		}
		return buscado;
	}
	/**
	 * Saca un carro del parqueadero 5 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP5 (String matricula)
	{
		Carro buscado = null;
		if(!park5.estaVacio()){
			Iterator itr = park5.iterator();
			while(itr.hasNext())
			{

				Carro temp = (Carro)itr.next();
				if(temp.darMatricula().equals(matricula)){
					buscado = temp;
					park5.pop();
				}
				else 
				{
					temporal.push(park5.pop());
				}
			}
			Iterator itr2 = temporal.iterator();
			while (itr.hasNext())
			{
				park5.push(temporal.pop());
				Carro temp2 = (Carro) itr.next();
			}
		}
		return buscado;
	}
	/**
	 * Saca un carro del parqueadero 6 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP6 (String matricula)
	{

		Carro buscado = null;
		if(!park6.estaVacio()){
			Iterator itr = park6.iterator();
			while(itr.hasNext())
			{

				Carro temp = (Carro)itr.next();
				if(temp.darMatricula().equals(matricula)){
					buscado = temp;
					park6.pop();
				}
				else 
				{
					temporal.push(park6.pop());
				}
			}
			Iterator itr2 = temporal.iterator();
			while (itr.hasNext())
			{
				park6.push(temporal.pop());
				Carro temp2 = (Carro) itr.next();
			}
		}
		return buscado;
	}
	/**
	 * Saca un carro del parqueadero 7 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP7 (String matricula)
	{
		Carro buscado = null;
		if(!park7.estaVacio()){
			Iterator itr = park7.iterator();
			while(itr.hasNext())
			{

				Carro temp = (Carro)itr.next();
				if(temp.darMatricula().equals(matricula)){
					buscado = temp;
					park7.pop();
				}
				else 
				{
					temporal.push(park7.pop());
				}
			}
			Iterator itr2 = temporal.iterator();
			while (itr.hasNext())
			{
				park7.push(temporal.pop());
				Carro temp2 = (Carro) itr.next();
			}
		}
		return buscado;
	}
	/**
	 * Saca un carro del parqueadero 8 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP8 (String matricula)
	{

		Carro buscado = null;
		if(!park8.estaVacio()){
			Iterator itr = park8.iterator();
			while(itr.hasNext())
			{

				Carro temp = (Carro)itr.next();
				if(temp.darMatricula().equals(matricula)){
					buscado = temp;
					park8.pop();
				}
				else 
				{
					temporal.push(park8.pop());
				}
			}
			Iterator itr2 = temporal.iterator();
			while (itr.hasNext())
			{
				park8.push(temporal.pop());
				Carro temp2 = (Carro) itr.next();
			}
		}
		return buscado;
	}
	/**
	 * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
	 * la tarifa es de $25 por minuto
	 * @param car recibe como parametro el carro que sale del parqueadero
	 * @return el valor que debe ser cobrado al cliente 
	 */
	public double cobrarTarifa (Carro car)
	{
		Date salida = new Date();
		long salidaTiempo = salida.getTime();
		long diffMins = (salidaTiempo - car.darLlegada())/(60*1000) % 60;
		int tarifa = (int) diffMins*25;
		return tarifa;	
	}
}
